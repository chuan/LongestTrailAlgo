/******Functions that used in LongestTrail.cpp********
1. Contains: to return the logic test if a node is included in a trail
2. nextnode: 
3. Calculate_d: calculate the length of the trail 
*********************/
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include "Node_Info.h"

int Contains(vector<int> *v,int number){
	for(int i=0;i<v->size();i++){
		if(v->at(i)==number){
			return 1;
		}
	}
	return 0;
}
void Cal_d(int v, int u, int *d,std::vector<int> &path,map<int,Node_Info> &Map,int *ancestor,int cyc_not_include){
		int x=ancestor[u];
		if(v!=x){
			Cal_d(v,x,d,path,Map,ancestor,cyc_not_include);
			Node_Info X_info=Map.find(x)->second;
			if(X_info.If_Existed_Cycle_by_length(u,cyc_not_include)){ 
				Cycle C=X_info.Return_Cycle_by_length(u,cyc_not_include);
				*d+=C.Getlength();
				vector<int> path_cyc=C.Getpath();
				path.insert(path.begin(),path_cyc.begin(),path_cyc.end());
			}
			else{
				*d=*d+1;
			}
			path.insert(path.begin(),x);
		}
		else{
			*d=1;
			path.clear();
			path.insert(path.begin(),v);
		}
}

int nextnode(int *ancestor,int v,int u){
	int father=u;
	if(v==u){
		return v;
	}
	else{
		if(ancestor[u]==v){
			return u;
		}
		else{
			do{
				father=ancestor[father];
				if(father==0){
					return nextnode(ancestor,u,v);				}
			}while(v!=ancestor[father]);
			return father;
		}
	}
}
/**********Calculate the trail between node v and u***************/
void Calculate_d(int v, int u, int *d,std::vector<int> &path,map<int,Node_Info> &Map, int *ancestor,int cyc_not_include){
	*d=0;
	path.clear();
	Cal_d(v,u,d,path,Map,ancestor,cyc_not_include);
	reverse(path.begin(),path.end());
	Node_Info V=Map.find(v)->second;
	int next=nextnode(ancestor, v,u);
	if(V.If_Existed_Cycle_by_nextnode(next)){
		Cycle *C=V.Return_Cycle_by_nextnode(next);
		if(1+C->Getlength()>2*(*d)){
			*d=1+C->Getlength()-*d;
			
			path.clear();
			std::vector<int> path_cycle = C->Getpath();
			reverse(path_cycle.begin(),path_cycle.end()); 
			path.assign(path_cycle.begin(),path_cycle.end());
			
			path.insert(path.begin(),v);
			path.pop_back();
		}
	}
	if(V.If_Existed_Cycle_by_length(next,cyc_not_include)){
		Cycle C=V.Return_Cycle_by_length(next,cyc_not_include);
		if(C.Getlength()>0){
			vector<int> path_cyc=C.Getpath();
			*d+=C.Getlength()-1;
			if(path.at(0)==v){

				path.insert(path.begin(),path_cyc.begin(),path_cyc.end());
			}
			else{
				path.insert(path.begin(),v);

				path.insert(path.begin(),path_cyc.begin(),path_cyc.end());
			}
		}
	}
}


