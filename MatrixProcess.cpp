/*This code includes functions about manipulations on contact maps:
1.OpenFile
2.NoiseIntroduction
3.RandomPermutation
4.WritetoFile
********************************/
#include <iostream>
#include <fstream>
#include <string.h>
#include "Header.h"
#include <map>
#include <set>
#include <vector>
#include <time.h>
#include <stdlib.h>

/******************************************************
*****Read input contact map **********************
@param f: the input file	
@param argv: inputs of command line
@param m: the size of contact map
@param nz: the number of non zeros in contact map
return a map with all info of contact map
map structure: <int:number of node i, set<int>: neighbours of node i> 
****************************************************/
std::map<int,std::set<int> > OpenFile(FILE *f,char **argv,int *m,int *nz){
	char filename[100];
	int x,y;
	int n;
	float poid;
	strcpy(filename,argv[1]);


	if( (f = fopen(argv[1], "r")) == NULL)
	  { fprintf(stderr,"Erreur: file does not exist\n ");  
	    exit(1);}

    	if(fscanf(f,"%d %d %d\n",&n,m,nz)!=3){   
		fprintf( stderr, "Expected at least three numbers as input\n");
		exit(1); 
  	  }

	std::cout<<"The input contact map is of size: "<< *m<<"\n";
	std::cout<<"--------------------------------------------\n";    	
	n=*m;
  
	std::map<int,std::set<int> > Connected_Check;

		while(fscanf(f,"%d%d%*[^\n]",&x,&y)!=EOF){
			if(x!=y){
				std::map<int,std::set<int> >::iterator it;
				std::map<int,std::set<int> >::iterator it1;
				it=Connected_Check.find(x);
				it1=Connected_Check.find(y);
				if(it==Connected_Check.end()){
					std::set<int> Edges1;
					Connected_Check.insert(std::pair<int,std::set<int> >(x,Edges1));
				}
				if(it1==Connected_Check.end()){
					std::set<int> Edges2;
					Connected_Check.insert(std::pair<int,std::set<int> >(y,Edges2));
				}

				if(	(Connected_Check.find(x)->second.find(y)==Connected_Check.find(x)->second.end())){
					std::map<int,std::set<int> >::iterator itn=Connected_Check.find(x);
					std::map<int,std::set<int> >::iterator itn1=Connected_Check.find(y);
					itn->second.insert(y);
					itn1->second.insert(x);

				}
				else{
					printf("There is an duplication in the file of vertices: %d,%d\n",x,y);
				}
			}
		}
		return Connected_Check;
}

/*************************************************
***** Noise introduction **********************	
@param m: the size of the contact map
@param percent_of_distrub: the percentage of noises
@param Connected: contact map to be updated to a noisy one 
*************************************************/	
void NoiseIntroduction(int m, float percent_of_disturb,std::map<int,std::set<int> > &Connected){

	int times = percent_of_disturb * m; //Decide the number of interactions to achieve noise introduction

	for (int t =0 ;t<times; t++){

		int node = rand()%m+1; //Choose a node in the contact map randomly
		std::map<int,std::set<int> >::iterator it = Connected.find(node); //Have the neighbours of this node
		if(it!=Connected.end()){
			std::set<int> edges = it->second;
			int size_set_edges = edges.size();
			if(size_set_edges>0){
				// Choose an edge to delete
				int random_choose = rand()%size_set_edges; 
				std::set<int>::iterator itedges = edges.begin();
				std::advance(itedges,random_choose);
				int Edge_to_Delete = *(itedges);
				// Choose an edge to add 
				int Edge_to_Add;
				do{
					Edge_to_Add = rand()%m+1;
				}while(edges.find(Edge_to_Add)!=edges.end()||Edge_to_Add==node);
				
				// Noise introduction: update contact map with noises
				Connected.find(node)->second.erase(Edge_to_Delete);
				Connected.find(node)->second.insert(Edge_to_Add);
				Connected.find(Edge_to_Delete)->second.erase(node);
				Connected.find(Edge_to_Add)->second.insert(node);

				
			}
			else{
				if(t>0){
					t--;
				}
				else{
					t = 0;
				}
			}
		}
		else{
			if(t>0){
				t--;
			}
			else{
				t = 0;
			}
		}
	}
}

/*************************************************
***** Random Permutation **********************	
@param Connected_Check: noisy contact map
@param n: the size of contact map
return
@param Adj: a permuted and noisy contact map 
and
the new assignment to each residue by a vector of int
*************************************************/	
std::vector<int> RandomPermutation(noeud *Adj[],std::map<int,std::set<int> > Connected_Check, int n){
	std::vector<int> affectation(n+1,0);
	for(int i=1;i<=n;i++) affectation[i]=i;
	//Permutation process: give a new assignment to each residue
	srand (time(NULL));
	for(int i=1;i<=n;i++){
		int r1=rand()%n+1;
		int temp=affectation[r1];
		affectation[r1]=affectation[i];
		affectation[i]=temp;
	}
	// Construct a permuted contact map 
    	int c;
    	for(c=1;c<=n;c++){
      		Adj[c]=z1;
    	}
	
	std::map<int,std::set<int> >::iterator itmap;
    	noeud *t;
	for(itmap = Connected_Check.begin();itmap!=Connected_Check.end();++itmap){
		int number_vetex = itmap->first;
		int v1 = affectation[number_vetex];
		std::set<int> edges = itmap->second;
		for(std::set<int>::iterator itedges = edges.begin();itedges!= edges.end(); ++itedges){
			int number_edge = *itedges;
			int v2 = affectation[number_edge];
			t=(noeud*)malloc(sizeof *t);
			t->s=v2;t->suivant=Adj[v1];Adj[v1]=t;
		}
	}
	return affectation;
}
/********************************************************
* Write the contact map obtained by the program to output file *
@param f: output file 
@param Connect_Check: noisy contact map
@param aff_rand: the random assignment to residues for building permuted and noisy contact map
@param affect_best: the assignment obtained by the longest Trail algo for permuted and noisy contact map
*********************************************************/
void WritetoFile(std::ofstream& f, std::map<int, std::set<int> > Connected_Check, std::vector<int> aff_rand, int* affect_best){
	std::map<int, std::set<int> >::iterator itmap;
	if (!f.is_open())
	{
		std::cout << "Unable to open file";
		exit(1);
	}
	for (itmap = Connected_Check.begin(); itmap != Connected_Check.end(); ++itmap){
		int number_vetex = itmap->first;
		int v1 = affect_best[aff_rand[number_vetex]]; 
		std::set<int> edges = itmap->second;
		for (std::set<int>::iterator itedges = edges.begin(); itedges != edges.end(); ++itedges){
			int number_edge = *itedges;
			if (number_vetex < number_edge){
				int v2 = affect_best[aff_rand[number_edge]];
				if (v1<v2) f << "(" << v1 << "," << v2 << ") ";
				else f << "(" << v2 << "," << v1 << ") ";
			}
		}
	}
	f << "\n";
}
